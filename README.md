# Laravel Rest Api

> Belajar [Basic Rest Api](https://www.youtube.com/playlist?list=PLEgI20pG1Dqz0ZRjerU_zpVbSrLSWdaLS) di Channel Youtube [Kawan Koding](https://www.youtube.com/channel/UChccjG2gYrS-y9yUteVV3Mg)

## Setup

1. Clone this repo

```bash
git clone https://bitbucket.org/arisca-abdullah/kk-laravel-rest-api.git

cd kk-laravel-rest-api
```

2. Install dependencies

```bash
composer install
```

3. Setup .env file

```bash
cp .env.example .env
```

4. Set up your database on .env file

```env
DB_CONNECTION=mysql
DB_HOST=127.0.0.1
DB_PORT=3306
DB_DATABASE=quotes
DB_USERNAME=root
DB_PASSWORD=
```

5. Create database on your machine
6. Run migration

```bash
php artisan migrate
```

7. Serve app

```bash
php artisan serve
```
