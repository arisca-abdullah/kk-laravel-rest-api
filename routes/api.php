<?php

Route::post('register', 'Api\RegisterController@action');
Route::post('login', 'Api\LoginController@action');
Route::get('user', 'Api\UserController@me')->middleware('auth:api');
Route::apiResource('quote', 'Api\QuoteController')->middleware('auth:api');